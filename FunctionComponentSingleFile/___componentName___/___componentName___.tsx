interface ___ComponentName___Props {}

export const ___Component_Name___ = (
  props: ___ComponentName___Props
): JSX.Element => {
  return <h1>___ComponentName___</h1>;
};
