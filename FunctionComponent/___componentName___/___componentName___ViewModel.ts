import { ___ComponentName___Props } from "./___componentName___Interfaces";

interface ___ComponentName___ViewModel {}

export const use___ComponentName___ViewModel = (
  props: ___ComponentName___Props
): ___ComponentName___ViewModel => {
  return {};
};
