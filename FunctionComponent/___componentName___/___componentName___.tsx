import { ___ComponentName___Props } from "./___componentName___Interfaces";
import { use___ComponentName___ViewModel } from "./___componentName___ViewModel";

export const ___Component_Name___ = (
  props: ___ComponentName___Props
): JSX.Element => {
  const viewModel = use___ComponentName___ViewModel(props);

  return <h1>___ComponentName___</h1>;
};
