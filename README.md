# NWON - VSCode Templates

This are templates that are meant to be used with the VsCode extension [yuanhjty.code-template-tool](https://github.com/yuanhjty/code-template-tool) and should simplify the creation of react components.

The idea is to add this repository as a git submodule wherever you want to import the templates from. A good place is the `.vscode` folder. You can add the sub module by running:

```shell
git submodule add git@gitlab.com:nwon-group/nwon-tooling/nwon-react-templates.git .vscode/templates
```

Just remember to point to the cloned repository in your `.workspace` file. For example:

```json
{
  "codeTemplateTool.templatesPath": "{workspace}/.vscode/templates"
}
```

## Templates

Currently we provide the following templates:

- Context provider: **[ContextProvider](./ContextProvider/___YourConcern___ContextProvider.tsx)**
- Function component: **[FunctionComponent](./FunctionComponent/___componentName___/___componentName___.tsx)**
- Function component with ViewModel: **[FunctionComponentWithoutViewModel](./FunctionComponentWithoutViewModel/___componentName___/___componentName___.tsx)**
