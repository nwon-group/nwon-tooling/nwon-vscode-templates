import { ___componentName___Props } from "./___componentName___Interfaces";

export const ___Component_Name___ = (
  props: ___componentName___Props
): JSX.Element => {
  return <h1>___componentName___</h1>;
};
