import { assertTruthy } from "@nvon/baseline";
import { ComponentWithChildrenProps, FullContext } from "@nvon/react-toolbox";
import { createContext, useContext, useMemo, useState } from "react";

/**
 * Data that is stored in the context
 */
export interface ___YourConcern___Data {
  firstName: string;
  lastName: string;
}

/**
 * Data that is calculated/composed based on the actual data of the context for convenience reasons
 */
export interface ___YourConcern___Derived {
  fullName: string;
}

export type ___YourConcern___Context = FullContext<
  ___YourConcern___Data,
  ___YourConcern___Derived
>;

interface ___YourConcern___ContextProviderProps
  extends ComponentWithChildrenProps {
  initialState?: ___YourConcern___Data;
}

const contextObject = createContext<___YourConcern___Context | null>(null);

export const use___YourConcern___Context = ():
  | ___YourConcern___Context
  | never => {
  const context = useContext(contextObject);

  assertTruthy(
    context,
    "___YourConcern___ context has not been initialized. Is your component wrapped in a provider?"
  );

  return context;
};

export const ___YourConcern___ContextProvider = ({
  children,
  initialState,
}: ___YourConcern___ContextProviderProps): JSX.Element => {
  const [data, set] = useState<___YourConcern___Data>(
    initialState || {
      firstName: "Peter",
      lastName: "Pan",
    }
  );

  const fullContext: ___YourConcern___Context = useMemo(
    () => ({
      data,
      set,
      derived: {
        fullName: `${data.firstName} ${data.lastName}`,
      },
    }),
    [data]
  );

  return (
    <contextObject.Provider value={fullContext}>
      {children}
    </contextObject.Provider>
  );
};
